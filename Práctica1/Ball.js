var GameEngine = (function(GameEngine) {
  
  let PI2 = 2*Math.PI;

  class Ball {
    constructor(x, y, size) {
      this.x = x;
      this.y = y;
      this.size = size;

      this.speed = 600;
      this.angle = this.getRandomAngle();

      this.vx = Math.cos(this.angle) * this.speed;
      this.vy = Math.sin(this.angle) * this.speed;
    }

    update(elapsed) {
      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
    }

    render(ctx) {
      ctx.fillStyle = "#ff00ff";
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.size, 0, PI2);
      ctx.fill();  
    }

    updateVXY(){
      this.angle = this.getRandomAngle();
      this.vx = Math.cos(this.angle) * this.speed;
      this.vy = Math.sin(this.angle) * this.speed;
    }

    getRandomAngle(){
      let angleR = (360 * Math.random()) * Math.PI/180;
      while(!((angleR >= 2.5 && angleR <= 2.9) || 
        (angleR >= 3.5 && angleR <= 3.9) || 
        (angleR >= 5.5 && angleR <= 5.9) || 
        (angleR >= 6.5 && angleR <= 7.9))){

        angleR = (360 * Math.random()) * Math.PI/180;
      }
      return angleR;
    }
  }

  GameEngine.Ball = Ball;
  return GameEngine;
})(GameEngine || {})