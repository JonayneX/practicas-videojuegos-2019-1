var GameEngine = (function(GameEngine) {

  let pausa;
  let ini = "Presione ESPACIO para jugar una ronda.";
  let juego = "PONG by Jonayne.";

  let cw;
  let ch;

  let rect1_x;
  let rect1_y;
  let rect2_x;
  let rect2_y;

  let rect_w;
  let rect_h;

  let puntaje_1 = 0;
  let puntaje_2 = 0;

  let Key = {

    _pressed : {},
    
    W: 87,
    UP: 38,
    S: 83,
    DOWN: 40,
    ESPACIO: 32,

    isPress: function(keyCode) {
      return this._pressed[keyCode];
    },
    onKeyDown: function(keyCode) {
      this._pressed[keyCode] = true;
    },
    onKeyUp: function(keyCode) {
      delete this._pressed[keyCode];
    }

  }

  class Game {
    constructor(ctx) {

      cw = ctx.canvas.width;
      ch = ctx.canvas.height;

      this.ctx = ctx;

      let radius = 15;
      this.ball = new GameEngine.Ball(cw/2, ch/2, radius);

      pausa = true;

      rect_w = 30;
      rect_h = 60;

      rect1_x = 0;
      rect1_y = ch/2;

      rect2_x = cw;
      rect2_y = ch/2
      
      window.addEventListener("keydown", function(evt) {
        Key.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        Key.onKeyUp(evt.keyCode);
      });

    }

    processInput() {
        if(!pausa){

          if (Key.isPress(Key.W)) {
            rect1_y -= 8;
          }
          if (Key.isPress(Key.S)) {
            rect1_y += 8;
          }
          if (Key.isPress(Key.UP)) {
            rect2_y -= 8;
          }
          if (Key.isPress(Key.DOWN)) {
            rect2_y += 8;
          }

        }else{

            if (Key.isPress(Key.ESPACIO)) {
            pausa = false;

          }
        }
    }

    update(elapsed) {
        if(!pausa){

          if(puntaje_1 == 5 || puntaje_2 == 5){
            pausa = true;
          }

          if (rect1_y < rect_h/2) {
            rect1_y = rect_h/2;
          }
          if (rect1_y > ch - rect_h/2) {
            rect1_y = ch - rect_h/2;
          }
          if (rect2_y < rect_h/2) {
            rect2_y = rect_h/2;
          }
          if (rect2_y > ch - rect_h/2) {
            rect2_y = ch - rect_h/2;
          }

          if (this.ball.x <= this.ball.size) {
            pausa = true;
            puntaje_2 = puntaje_2 + 1;
            ini = "Presione ESPACIO para continuar";
            this.ball.x = cw/2;
            this.ball.y = ch/2; 
            rect1_y = ch/2;
            rect2_y = ch/2;
            this.ball.updateVXY();
          }
          if (this.ball.x >= cw-this.ball.size) {
            pausa = true;
            puntaje_1 = puntaje_1 + 1;
            ini = "Presione ESPACIO para continuar";
            this.ball.x = cw/2;
            this.ball.y = ch/2;
            rect1_y = ch/2;
            rect2_y = ch/2;
            this.ball.updateVXY();
          }          
          if (this.ball.x < this.ball.size + (rect_w/2) && 
                (this.ball.y >= (rect1_y - (rect_h/2)) && this.ball.y <= (rect1_y + (rect_h/2)))) { 

            this.ball.vx *= (-1.08);
            this.ballx = this.ball.size + rect_w;

          }
          if (this.ball.x > cw-this.ball.size - (rect_w/2) && 
            (this.ball.y >= (rect2_y - (rect_h/2)) && this.ball.y <= (rect2_y + (rect_h/2)))) { 
            
            this.ball.vx *= (-1.08);
            this.ball.x = cw-this.ball.size - rect_w;
          }

          if (this.ball.y < this.ball.size) {
            this.ball.vy *= -1;
            this.ball.y = this.ball.size;
          }
          if (this.ball.y > ch-this.ball.size) {
            this.ball.vy *= -1;
            this.ball.y = ch-this.ball.size;
          }

          this.ball.update(elapsed);
        }

    }

    render() {

        this.ctx.fillStyle = "white";
        this.ctx.strokeStyle = "black";
        this.ctx.lineWidth = 5;
        this.ctx.lineJoin = "round";
        

        if(!pausa){

            this.ctx.clearRect(0, 0, cw, ch);

            this.ctx.fillStyle = "red";
            this.ctx.beginPath();

            this.ctx.fillRect(rect1_x - rect_w/2, rect1_y-rect_h/2, rect_w, rect_h);
            this.ctx.fillRect(rect2_x - rect_w/2, rect2_y-rect_h/2, rect_w, rect_h);

            this.ctx.font = "bold 50px Monospace";
            this.ctx.beginPath();
            this.ctx.strokeText(puntaje_1.toString(), 150, 70);
            this.ctx.fillText(puntaje_1.toString(), 150, 70);

            this.ctx.strokeText(puntaje_2, 820, 70);
            this.ctx.fillText(puntaje_2, 820, 70);

            this.ball.render(this.ctx);

        }else{
            if(puntaje_1 == 5){
              this.ctx.fillStyle = "red";
              this.ctx.strokeStyle = "blue";
              this.ctx.strokeText("Ganó jugador 1!", 300, 400);
              puntaje_1 = 0;
              puntaje_2 = 0;
            }else if(puntaje_2 == 5){
              this.ctx.fillStyle = "white";
              this.ctx.strokeStyle = "red";
              this.ctx.strokeText("Ganó jugador 2!", 300, 400);
              puntaje_1 = 0;
              puntaje_2 = 0;
            }
            this.ctx.font = "bold 20px Monospace";
            this.ctx.strokeText(ini, 300, 300);
            this.ctx.fillText(ini, 300, 300);

            this.ctx.fillStyle = "red";
            this.ctx.strokeStyle = "blue";
            this.ctx.font = "bold 50px Monospace";
            this.ctx.strokeText(juego, 300, 100);
            this.ctx.fillText(juego, 300, 100);
        }

    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})