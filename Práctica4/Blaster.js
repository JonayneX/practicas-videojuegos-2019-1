var GameEngine = (function(GameEngine) {
  let gravity = 16;
  const PI_180 = Math.PI/180;
  const PI2 = 2*Math.PI;

  class Blaster {
    constructor(x, y, w, h, dire) {
      this.sprite = new GameEngine.Sprite(x, y, w, h, "images/enemy02.png", 4, 16, 16);
      this.bullets = [];
      this.bullets_speed = 200;
      this.dire = dire;
      for(let i=0; i<4; i++){
        var angle = this.dire*(PI_180*(140+(i*30)));
        this.bullets.push({x_: x,
                          y_: y,
                          vx: Math.cos(angle) * this.bullets_speed,
                          vy: Math.sin(angle) * this.bullets_speed,
                          w2: w/2,
                          h2: h/2,
                          lanzada: false});
      }

      this.tiempo_cubierto = 2;
      this.tiempo_aux = 0;
      this.atacar = false;
      this.tiempo_ent_lanz = 0.75;

      this.sprite.direction = dire;
      this.pos_oriX = x;
      this.pos_oriY = y;
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;

      this.tam_bala = 5;

      this.dead = false;

      this.frameCounter = 0;
      this.framesPerChange = 5;
      this.numFrameAnimation = 4;

      this.w_2 = w/2;
      this.h_2 = h/2;

      this.speed = 120;
      this.jump_heigth = 366;
      this.inFloor = false;

      this.vx = -25;
      this.vy = 0;
      this.canJump = true;

      this.active = false;
    }

    comebackToOri(){
      this.x = this.pos_oriX;
      this.y = this.pos_oriY;
    }

    lerp(v0, v1, t) {
      return v0 + (v1 - v0)*t;
    }

    update(elapsed, level, cw, ch, camera, player) {
      if (this.active && (!this.dead)) {

        this.tiempo_aux+= elapsed;

        if(this.tiempo_aux >= this.tiempo_cubierto && !this.atacar){
          this.frameCounter = (this.frameCounter +1)%(this.framesPerChange*this.numFrameAnimation);
          this.sprite.currentFrame = parseInt(this.frameCounter/this.framesPerChange);
        }

        if(this.sprite.currentFrame == 3 && !this.atacar)
          this.atacar = true;

        if(this.atacar){
          if(this.tiempo_aux >= this.tiempo_cubierto + 3.5){
            this.atacar = false;
            this.tiempo_aux = 0;
            this.sprite.currentFrame = 0;
            this.frameCounter = 0;
            for(let i=0; i<4; i++){
              this.bullets[i].x_ = this.x;
              this.bullets[i].y_ = this.y;
              this.bullets[i].lanzada = false;
            }

          }else{

            this.verifica_lanzamientos();

            for(let i=0; i<4; i++){
              if(this.bullets[i].lanzada){
                this.bullets[i].x_ += this.bullets[i].vx * elapsed;
                this.bullets[i].y_ += this.bullets[i].vy * elapsed;
                this.checkCollisionPlayer(player, this.bullets[i]);
                if(this.bullets[i].x_ < camera.x - cw || this.bullets[i].y_ < camera.y - ch)
                  this.bullets[i].lanzada = false;
              }
            }
          }
        }

        this.inFloor = false;

        this.sprite.x = this.x;
        this.sprite.y = this.y;

          
        this.checkCollisionWalls(cw, ch, camera);
      }else if(this.active){
        this.checkCollisionWalls(cw, ch, camera);
      }
    }

    verifica_lanzamientos(){
      if(this.tiempo_aux >= this.tiempo_cubierto + this.tiempo_ent_lanz 
        && this.tiempo_aux <= this.tiempo_cubierto + this.tiempo_ent_lanz*2){
        this.bullets[0].lanzada = true;
      }else if(this.tiempo_aux >= this.tiempo_cubierto + this.tiempo_ent_lanz*2 
        && this.tiempo_aux <= this.tiempo_cubierto + this.tiempo_ent_lanz*3){
        this.bullets[1].lanzada = true;
      }else if(this.tiempo_aux >= this.tiempo_cubierto + this.tiempo_ent_lanz*3 
        && this.tiempo_aux <= this.tiempo_cubierto + this.tiempo_ent_lanz*4){
        this.bullets[2].lanzada = true;
      }else{
        this.bullets[3].lanzada = true;
      }
    }
    
    checkCollisionWalls(cw, ch, camera) {
      if ((this.x < camera.x - cw || this.x > camera.x + cw) ||
        (Math.abs(this.y - camera.y) > 125)) {
        this.active = false;
        this.dead = false;
      }
    }

    checkCollisionPlayer(player, b) {
      if(Math.abs(b.x_ - player.x) < b.w2 + player.w_2 && 
           Math.abs(b.y_ - player.y) < b.h2 + player.h_2 ){
        if(!player.hurt)
          player.got_hit();
      }
    }

    render(ctx) {
      if (this.active && (!this.dead)) {
        this.sprite.render(ctx);
        for(let i=0; i<4; i++){
          if(this.bullets[i].lanzada){
            ctx.fillStyle = "blue";
            ctx.beginPath();
            ctx.arc(this.bullets[i].x_, this.bullets[i].y_, this.tam_bala, 0, PI2);
            ctx.fill();
          }
        }
      }
    }
  }

  GameEngine.Blaster = Blaster;
  return GameEngine;
})(GameEngine || {})