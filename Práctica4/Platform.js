var GameEngine = (function(GameEngine) {

  class Platform {
    constructor(x, y, w, h) {
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
      this.w_2 = w/2;
      this.h_2 = h/2;
      this.type = "Platform";
      
      this.currentFrame = 0;
    }

    update(elapse) {
    }

    render(ctx) {
    }
  }

  GameEngine.Platform = Platform;
  return GameEngine;
})(GameEngine || {})