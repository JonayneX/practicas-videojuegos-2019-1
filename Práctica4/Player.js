var GameEngine = (function(GameEngine) {
  let gravity = 16;

  let KEY = GameEngine.KEY;
  let inv_t_aux = 0;
  let inv_t = 1.4;
  class Player extends GameEngine.Sprite {
    constructor(x, y, w, h) {
      super(x, y, w, h, "images/megaman.png", 14, 32, 32);

      this.frameCounter = 0;
      this.framesPerChange = 6;

      this.ladderCounter = 0;
      this.ladderFramesPerChange = 12;

      this.w_2 = w/2;
      this.h_2 = h/2;

      this.shootingFr = 0.3;
      this.auxShootingFr = 0;

      this.speed = 100;
      this.jump_heigth = 290;
      this.inFloor = false;

      this.vx = 0;
      this.vy = 0;
      this.canJump = true;

      this.shooting = false;
      this.weapon = [];

      for(let i = 0; i < 15; i++){
        this.weapon.push(new GameEngine.Bullet(x, y, 10, 10));
      }

      this.canShoot = true;
      this.shootAnim = false;

      this.hurt = false;

      this.vidas = []

      for(let i= 0; i<14; i++){
        this.vidas.push({live: true});
      }

      this.dead = false;
    }

    shot(){
      for(let i = 0; i < 15; i++){
        if(!this.weapon[i].isAlive){
          if(this.direction == 1)
            this.weapon[i].shot(this.x, this.y, 16, 3, this.vx, 1);
          else
            this.weapon[i].shot(this.x, this.y, -16, 3, this.vx, -1);
          return;
        }
      }
    }

    got_hit(){
      this.hurt = true;
      for(let i= 0; i < 13; i++){
        if(this.vidas[i].live){
          this.vidas[i].live = false;
          return;
        }
      }
      this.dead = true;
    }

    processInput() {
      this.vx = 0;
      this.ladderVy = 0;
      this.tryGrabLadder = false;
      this.shooting = false;

      if (KEY.isReleased(KEY.X_KEY)) {
        this.canJump = true;
      }
      if (KEY.isReleased(KEY.Z_KEY)) {
        this.canShoot = true;
      }
      if ((KEY.isPress(KEY.X_KEY)) && (this.canJump) && (this.inFloor)) {
        this.vy = -this.jump_heigth;
        this.canJump = false;
      }

      if ((KEY.isPress(KEY.X_KEY)) && (this.inLadder)) {
        this.inLadder = false;
        this.w = 32;
        this.w_2 = 8;
      }

      if ((KEY.isPress(KEY.Z_KEY)) && (this.canShoot)) {
        this.shooting = true;
        this.shootAnim = true;
        this.shot();
      }

      if (KEY.isPress(KEY.LEFT)) {
        this.vx = -this.speed;
        this.direction = -1;
      }

      if (KEY.isPress(KEY.RIGHT)) {
        this.vx = this.speed;
        this.direction = 1;
      }

      if (KEY.isPress(KEY.UP)) {
        this.tryGrabLadder = true;
        this.ladderVy = -this.speed/2;
        if (this.inLadder) {
          if (((this.ladderCounter++)%this.ladderFramesPerChange) === 0) {
            this.direction *= -1;
          }
        }
      }
      if (KEY.isPress(KEY.DOWN)) {
        this.tryGrabLadder = true;
        this.ladderVy = this.speed/2;
        if (this.inLadder) {
          if (((this.ladderCounter++)%this.ladderFramesPerChange) === 0) {
            this.direction *= -1;
          }
        }
      }

    }

    setState() {
      if (!this.inLadder) {
        if(this.hurt){
          this.state = "hurt";
        }else if (this.vx !== 0) {
          this.state = "walking";
        }
        else if (this.inFloor) {
          this.state = "still";
        }
        if (!this.inFloor) {
          this.state = "jumping";
        }
      }
      else {
        this.state = "ladder";
      }
    }

    update(elapsed, enemies) {
      if(((this.x >= 650 && this.x <= 700) && this.y >= 2144)
        || ((this.x >= 1100 && this.x <= 1250) && this.y >= 1184))
        this.dead = true;

      if(this.hurt){
        inv_t_aux += elapsed;
        if(inv_t_aux >= inv_t){
          this.hurt = false;
          inv_t_aux = 0;
        }
      }

      if (this.shootAnim) {
        this.canShoot = false;
        this.auxShootingFr += elapsed;
        if (this.auxShootingFr >= this.shootingFr) {
          this.auxShootingFr = 0;
          this.canShoot = false;
          this.shootAnim = false;
        }
      }
      
      this.inFloor = false;
       if (!this.inLadder) {
        this.vy += gravity;

        super.update(elapsed);
      }
      else {
        this.vx = 0;
        this.vy = this.ladderVy;
        super.update(elapsed);
      }

      for(let i = 0; i < 15; i++){
        this.weapon[i].update(elapsed, enemies);
      }

    }
    
    render(ctx) {
      if(this.state == "hurt"){
        this.currentFrame = 13;
      }
      else if (this.state === "walking" && this.shootAnim) {
        this.frameCounter = (this.frameCounter+1)%(this.framesPerChange*4);
        let theFrame = parseInt(this.frameCounter/this.framesPerChange);
        if (theFrame === 3) {
          theFrame = 1;
        }
        this.currentFrame = 8 + theFrame;
      }
      else if (this.state === "walking") {
        this.frameCounter = (this.frameCounter +1)%(this.framesPerChange*4);
        let theFrame = parseInt(this.frameCounter/this.framesPerChange);
        if (theFrame === 3) {
          theFrame = 1;
        }
        this.currentFrame = 1 + theFrame;
      }
      else if (this.state === "still" && this.shootAnim) {

        this.currentFrame = 7;
      }
      else if (this.state === "still") {
        this.currentFrame = 0;
      }
      else if (this.state === "jumping" && this.shootAnim) {
        this.currentFrame = 11;
      }
      else if (this.state === "jumping") {
        this.currentFrame = 4;
      }
      else if (this.state === "ladder" && this.shootAnim) {
        this.currentFrame = 12;
      }
      else if (this.state === "ladder") {
        this.currentFrame = 5;
      }
      for(let i = 0; i < 15; i++){
        this.weapon[i].render(ctx);
      }
      super.render(ctx);
    }
  }

  GameEngine.Player = Player;
  return GameEngine;
})(GameEngine || {})