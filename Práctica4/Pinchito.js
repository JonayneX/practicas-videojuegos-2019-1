var GameEngine = (function(GameEngine) {

  class Pinchito {
    constructor(x, y, w, h) {
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;
      this.w_2 = w/2;
      this.h_2 = h/2;
      this.type = "Pinchito";
    }

  }

  GameEngine.Pinchito = Pinchito;
  return GameEngine;
})(GameEngine || {})