var GameEngine = (function(GameEngine) {
  const PI2 = 2*Math.PI;
  let KEY = GameEngine.KEY;

  class Bullet extends GameEngine.Sprite {
    constructor(x, y, w, h, speed=130, activeTime=2) {
      super(x, y, w, h, "images/bullet.png", 1, 1, 1, "notAnim");
      this.speed = speed;
      this.activeTime = activeTime;
      this.auxActiveTime = 0;
      
      this.w_2 = w/2;
      this.h_2 = h/2;

      this.vx = 0;
      this.vy = 0;

      this.x = 0;
      this.y = 0;

      this.isAlive = false;
    }

    processInput() {
    }

    shot(ox, oy, x, y, vx, dire){
      this.isAlive = true;
      this.x = ox + x;
      this.y = oy + y;
      if(dire == 1)
        this.vx = vx + this.speed;
      else
        this.vx = vx - this.speed;
    }

    update(elapsed, enemies) {
      if(this.isAlive){

        for(let i = 0, l = enemies.length; i < l; i++){
          if(!enemies[i].dead && enemies[i].active)
            this.checkCollision(enemies[i]);
        }

        this.x += this.vx * elapsed;

        this.auxActiveTime += elapsed;
        if (this.auxActiveTime >= this.activeTime) {
          this.isAlive = false;
          this.auxActiveTime = 0;
        }
        
        super.update(elapsed);     
      }
    }
    
    checkCollision(enemie) {

      if(Math.abs(this.x - enemie.x) < this.w_2 + enemie.w_2 && 
           Math.abs(this.y - enemie.y) < this.h_2 + enemie.h_2 ){
        this.isAlive = false;
        enemie.dead = true;
        enemie.active = false;
        enemie.comebackToOri(); 
      }
    }

    render(ctx) {
      if(this.isAlive){
        super.render(ctx);
      }
    }
  }

  GameEngine.Bullet = Bullet;
  return GameEngine;
})(GameEngine || {})