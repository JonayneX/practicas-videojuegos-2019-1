var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  let KEY = GameEngine.KEY;

  let time_paux = 0;
  let cuenta = false;
  
  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;
      this.inicio = true;
      this.cw_2 = cw/2;
      this.ch_2 = ch/2;
      this.camera = new GameEngine.Camera(cw/2, ch/2, cw, ch);

      // Un pixel en el mapa de Tiled (x,y) es (16x, 16y) en el canvas.
      this.player = new GameEngine.Player((16*8), (16*129), 32, 32);
      this.level = new GameEngine.Level();

      this.image_start = new Image();
      this.image_start.src = "images/startscreen.jpeg";

      this.image_lvl1 = new Image();
      this.image_lvl1.src = "images/lvl1-cutman.jpeg";

      this.image_game_over = new Image();
      this.image_game_over.src = "images/game_over.jpeg";

      // Los enemigos del nivel.
      this.enemies = [];
      this.enemies.push(new GameEngine.Blader_enemy((16*19), (16*125), 16, 16));
      this.enemies.push(new GameEngine.Blader_enemy((16*25), (16*127), 16, 16));
      this.enemies.push(new GameEngine.Blader_enemy((16*23), (16*129), 16, 16));
      this.enemies.push(new GameEngine.Blader_enemy((16*42), (16*124), 16, 16));
      this.enemies.push(new GameEngine.Blader_enemy((16*43), (16*126), 16, 16));
      this.enemies.push(new GameEngine.Blader_enemy((16*45), (16*129), 16, 16));
      this.enemies.push(new GameEngine.Blader_enemy((16*117), (16*3), 16, 16));
      this.enemies.push(new GameEngine.Blader_enemy((16*116), (16*7), 16, 16));
      this.enemies.push(new GameEngine.Blader_enemy((16*120), (16*5), 16, 16));

      this.enemies.push(new GameEngine.Blaster((16*51.5), (16*124.5), 16, 16, -1));
      this.enemies.push(new GameEngine.Blaster((16*59.5), (16*127.5), 16, 16, -1));
      this.enemies.push(new GameEngine.Blaster((16*61.5), (16*114.5), 16, 16, -1));
      this.enemies.push(new GameEngine.Blaster((16*55.5), (16*110.5), 16, 16, -1));
      this.enemies.push(new GameEngine.Blaster((16*57.5), (16*108.5), 16, 16, -1));
      this.enemies.push(new GameEngine.Blaster((16*52.5), (16*101.5), 16, 16, 1));
      this.enemies.push(new GameEngine.Blaster((16*55.5), (16*97.5), 16, 16, -1));
      this.enemies.push(new GameEngine.Blaster((16*59.5), (16*93.5), 16, 16, -1));
      this.enemies.push(new GameEngine.Blaster((16*55.5), (16*88.5), 16, 16, 1));
      this.enemies.push(new GameEngine.Blaster((16*59.5), (16*80.5), 16, 16, -1));
      this.enemies.push(new GameEngine.Blaster((16*58.5), (16*78.5), 16, 16, -1));

      this.enemies.push(new GameEngine.Super_cutter((16*56), (16*70), 16, 16, 0.4));
      this.enemies.push(new GameEngine.Super_cutter((16*57), (16*69), 16, 16, 0.8));
      this.enemies.push(new GameEngine.Super_cutter((16*55), (16*72), 16, 16, 1.3));
      this.enemies.push(new GameEngine.Super_cutter((16*56.5), (16*71), 16, 16, 1.9));

      this.enemies.push(new GameEngine.Super_cutter((16*88), (16*11), 16, 16, 0.4));
      this.enemies.push(new GameEngine.Super_cutter((16*88.5), (16*10), 16, 16, 0.8));
      this.enemies.push(new GameEngine.Super_cutter((16*87), (16*10.5), 16, 16, 1.3));
      this.enemies.push(new GameEngine.Super_cutter((16*90), (16*9.5), 16, 16, 1.9));    

      this.enemies.push(new GameEngine.Flea((16*70), (16*65), 16, 16));
      this.enemies.push(new GameEngine.Flea((16*69), (16*67), 16, 16));
      this.enemies.push(new GameEngine.Flea((16*72), (16*64), 16, 16));

      this.enemies.push(new GameEngine.Octopus_battery((16*84.5), (16*70.5), 16, 16, "V", -1));
      this.enemies.push(new GameEngine.Octopus_battery((16*91.5), (16*68.5), 16, 16, "H", -1));
      this.enemies.push(new GameEngine.Octopus_battery((16*86.5), (16*68.5), 16, 16, "V", 1)); 
      this.enemies.push(new GameEngine.Octopus_battery((16*93.5), (16*53.5), 16, 16, "H", -1)); 
      this.enemies.push(new GameEngine.Octopus_battery((16*85.5), (16*53.5), 16, 16, "V", -1)); 
      this.enemies.push(new GameEngine.Octopus_battery((16*87.5), (16*51.5), 16, 16, "H", -1));
      this.enemies.push(new GameEngine.Octopus_battery((16*84.5), (16*49.5), 16, 16, "H", 1));      
      this.enemies.push(new GameEngine.Octopus_battery((16*88.5), (16*41.5), 16, 16, "V", -1)); 
      this.enemies.push(new GameEngine.Octopus_battery((16*87.5), (16*40.5), 16, 16, "H", 1)); 
      this.enemies.push(new GameEngine.Octopus_battery((16*91.5), (16*36.5), 16, 16, "H", -1));
      this.enemies.push(new GameEngine.Octopus_battery((16*93.5), (16*35.5), 16, 16, "H", -1));
      this.enemies.push(new GameEngine.Octopus_battery((16*83.5), (16*25.5), 16, 16, "H", -1)); 
      this.enemies.push(new GameEngine.Octopus_battery((16*83.5), (16*22.5), 16, 16, "V", -1)); 
      this.enemies.push(new GameEngine.Octopus_battery((16*82.5), (16*19.5), 16, 16, "H", 1));
      this.enemies.push(new GameEngine.Octopus_battery((16*88.5), (16*23.5), 16, 16, "H", 1));  
      this.enemies.push(new GameEngine.Octopus_battery((16*92.5), (16*17.5), 16, 16, "V", -1));

      this.enemies.push(new GameEngine.Flying_shell((16*128), (16*22), 16, 16));
      this.enemies.push(new GameEngine.Flying_shell((16*128), (16*38), 16, 16));

      this.enemies.push(new GameEngine.Big_eye((16*127), (16*52), 32, 32));      


      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.player.processInput();
      if (KEY.isPress(KEY.ENTER_KEY) && this.inicio && !this.cutman_presen) {
        this.inicio = false;
        this.cutman_presen = true;
        cuenta = true;
      }
    }

    update(elapsed) {
      if(cuenta)
          time_paux += elapsed;
      if(!this.player.dead){
        
        if(!this.camera.cameraTransition){
          if(!this.inicio && !this.cutman_presen){
            if((this.player.tryGrabLadder) && (!this.player.inLadder)) {
                  this.checkCollisionLadders(this.player, this.level);
                }
          
            this.player.update(elapsed, this.enemies);
            
  
            if (this.player.inLadder) {
              this.checkInLadders(this.player, this.level);
            }
      
            this.checkCollisionPlatforms(this.player, this.level);
      
            if (this.player.inFloor) {
              this.player.inLadder = false;
            }
      
            this.player.setState();

            for (let i=0, l=this.enemies.length; i<l; i++) {
              
              if (Math.abs(this.camera.x - this.enemies[i].x) < this.cw_2 
                && (Math.abs(this.enemies[i].y - this.camera.y) < 125)) {
                this.enemies[i].active = true;
              }
              this.enemies[i].update(elapsed, this.level, cw, ch, this.camera, this.player);
            }
            if(!this.player.hurt)
              this.checkCollisionEnemies();
            this.checkCollisionWalls();
          }
  
        }
        this.camera.update(this.player, this.level, this.enemies); 
      }else{
        for (let i=0, l=this.enemies.length; i<l; i++) {
          this.enemies[i].comebackToOri();
        }
        cuenta = true;
      }
    }

    checkCollisionEnemies(){
      for (let i=0, l=this.enemies.length; i<l; i++) {
        if(!this.enemies[i].dead && this.enemies[i].active){
           if(Math.abs(this.player.x - this.enemies[i].x) < this.player.w_2 + this.enemies[i].w_2 && 
           Math.abs(this.player.y - this.enemies[i].y) < this.player.h_2 + this.enemies[i].h_2 ){
            this.player.got_hit();
           }
        }
      }
    }

    checkCollisionLadders(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);
      let ladder;

      // top
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y-1);
      if (ladder && (ladder.type === "Ladder") && (player.ladderVy < 0)) {
        this.player.x = ladder.x;
        this.player.y -= 4;
        this.player.inLadder = true;
        return;
      }

      //center
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y);
      if (ladder && (ladder.type === "Ladder")) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }

      // bottom
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y+1);
      if (ladder && (ladder.type === "Ladder") && (player.ladderVy > 0)) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }
    }

    checkInLadders(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y + player.h_2);
      let ladder;
      //center
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y);
      if (ladder && (ladder.type === "Ladder")) {
        this.player.inLadder = true;
        this.player.w = 32;
        this.player.w_2 = 8;
      }
      else {
        this.player.inLadder = false;
        this.player.w = 32;
        this.player.w_2 = 10;
      }
      
    }

    checkCollisionWalls() {
      if (this.player.x < this.camera.x - cw/2 + this.player.w_2) {
        this.player.x = this.camera.x - cw/2 + this.player.w_2;
      }
      if (this.player.x > this.camera.x +cw/2 -this.player.w_2) {
        this.player.x = this.camera.x +cw/2 - this.player.w_2;
      }

    }

    checkCollisionPlatforms(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);
      for(let i = -1; i <= 1; i++){
        for(let j = -1; j <= 1; j++){
          this.reactCollision(player, level.getPlatform(player_tile_pos.x+i, player_tile_pos.y+j));
        }

      }
      
    }

    reactCollision(player, platform, inStairs) {
      if(platform && platform.type === "Pinchito" &&
           Math.abs(player.x - platform.x) < player.w_2 + platform.w_2 && 
           Math.abs(player.y - platform.y) < player.h_2 + platform.h_2 ) {
        player.dead = true;
        return;
      }
      if ( platform && platform.type === "Platform" &&
           Math.abs(player.x - platform.x) < player.w_2 + platform.w_2 && 
           Math.abs(player.y - platform.y) < player.h_2 + platform.h_2 ) {

        let overlapX = (player.w_2 + platform.w_2) - Math.abs(player.x - platform.x);
        let overlapY = (player.h_2 + platform.h_2) - Math.abs(player.y - platform.y);

        if (overlapX < overlapY) {
          if (player.x - platform.x > 0) {
            player.x += overlapX;
          }
          else {
            player.x -= overlapX;
          }
        }
        else if (overlapX > overlapY) {
          if (player.y - platform.y > 0) {
            player.y += overlapY;
            if (player.vy < 0) {
              player.vy = 0;
            }
          }
          else {
            player.y -= overlapY;
            if (player.vy > 0) {
              player.inFloor = true;
              player.vy = 0;
            }
          }
        }
      }
    }

    render() {
      if(!this.inicio && !this.cutman_presen){
        if(!this.player.dead){
          this.ctx.fillStyle = "#5ad0e1";
          this.ctx.fillRect(0, 0, cw, ch);
    
          this.camera.applyTransform(this.ctx);
    
          this.level.render(this.ctx);
          this.player.render(this.ctx);
          
          this.camera.render(this.ctx, this.player.vidas);
  
          for (let i=0, l=this.enemies.length; i<l; i++) {
            this.enemies[i].render(this.ctx);
          }
  
          this.camera.releaseTransform(this.ctx);
        }else{
          if(this.image_game_over)
            this.ctx.drawImage(this.image_game_over, 0, 0);

          if(time_paux >= 4){
            cuenta = false;
            this.player = new GameEngine.Player((16*8), (16*129), 32, 32);
            this.camera = new GameEngine.Camera(cw/2, ch/2, cw, ch);
            time_paux = 0;
          }
        }

      }else if(this.inicio && !this.cutman_presen){

        if (this.image_start)
          this.ctx.drawImage(this.image_start, 0, 0);

      }else{

        if(this.image_lvl1)
          this.ctx.drawImage(this.image_lvl1, 0, 0);
        if(time_paux >= 2){
          cuenta = false;
          this.cutman_presen = false;
          time_paux = 0;
        }

      }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})