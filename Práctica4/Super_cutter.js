var GameEngine = (function(GameEngine) {
  let gravity = 16;

  class Super_cutter {
    constructor(x, y, w, h, ini) {
      this.sprite = new GameEngine.Sprite(x, y, w, h, "images/enemy03.png", 2, 16, 16);

      this.pos_oriX = x;
      this.pos_oriY = y;

      this.ini = ini;
      this.ini_aux = 0;
      this.br = true;

      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;

      this.dead = false;

      this.frameCounter = 0;
      this.framesPerChange = 8;
      this.numFrameAnimation = 2;

      this.w_2 = w/2;
      this.h_2 = h/2;

      this.speed = 120;
      this.jump_heigth = 366;
      this.inFloor = false;

      this.vx = -25;
      this.vy = 0;
      this.canJump = true;

      this.active = false;

      this.inFloor = true;

    }

    comebackToOri(){
      this.x = this.pos_oriX;
      this.y = this.pos_oriY;
      this.br = true;
    }

    lerp(v0, v1, t) {
      return v0 + (v1 - v0)*t;
    }

    update(elapsed, level, cw, ch, camera, player) {
      if (this.active && (!this.dead)) {

        if(this.br){
          this.ini_aux += elapsed;
          if(this.ini_aux >= this.ini){
            this.vy = -200;
            this.inFloor = false;
            this.br = false;
            this.ini_aux = 0;
          }
        }else{
          this.vy += gravity;
          this.x = this.lerp(this.x, player.x, 0.02);
          this.y += this.vy * elapsed;
        }

        this.sprite.direction = (this.x > player.x) ? 1 : -1;

        this.sprite.x = this.x;
        this.sprite.y = this.y;

        this.frameCounter = (this.frameCounter +1)%(this.framesPerChange*this.numFrameAnimation);
        this.sprite.currentFrame = parseInt(this.frameCounter/this.framesPerChange);  
        this.checkCollisionWalls(cw, ch, camera);
      }else if(this.active){
        this.checkCollisionWalls(cw, ch, camera);
      }
    }

    
    checkCollisionWalls(cw, ch, camera) {
      if ((this.x < camera.x - cw || this.x > camera.x + cw) ||
        (Math.abs(this.y - camera.y) > 125)) {
        this.active = false;
        this.dead = false;
      }
    }

    render(ctx) {
      if (this.active && (!this.dead)) {
        this.sprite.render(ctx);
      }
    }
  }

  GameEngine.Super_cutter = Super_cutter;
  return GameEngine;
})(GameEngine || {})