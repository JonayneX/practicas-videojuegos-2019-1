var GameEngine = (function(GameEngine) {
  let gravity = 16;
  let att_ida = true;

  class Blader_enemy {
    constructor(x, y, w, h) {
      this.sprite = new GameEngine.Sprite(x, y, w, h, "images/enemy01.png", 2, 16, 16);

      this.pos_oriX = x;
      this.pos_oriY = y;
      this.x = x;
      this.y = y;
      this.w = w;
      this.h = h;

      this.dead = false;

      this.frameCounter = 0;
      this.framesPerChange = 2;
      this.numFrameAnimation = 2;

      this.w_2 = w/2;
      this.h_2 = h/2;

      this.speed = 120;
      this.jump_heigth = 366;
      this.inFloor = false;

      this.vx = -25;
      this.vy = 0;
      this.canJump = true;

      this.active = false;

    }

    comebackToOri(){
      this.x = this.pos_oriX;
      this.y = this.pos_oriY;
    }

    lerp(v0, v1, t) {
      return v0 + (v1 - v0)*t;
    }

    update(elapsed, level, cw, ch, camera, player) {
      if (this.active && (!this.dead)) {
        this.inFloor = false;

        this.ataque(player);

        this.sprite.direction = (this.x > player.x) ? 1 : -1;

        this.sprite.x = this.x;
        this.sprite.y = this.y;

        this.frameCounter = (this.frameCounter +1)%(this.framesPerChange*this.numFrameAnimation);
        this.sprite.currentFrame = parseInt(this.frameCounter/this.framesPerChange);  
        this.checkCollisionWalls(cw, ch, camera);
      }else if(this.active){
        this.checkCollisionWalls(cw, ch, camera);
      }
    }

    ataque(player){
      var d = Math.abs(player.x - this.x);

      if(d > 35){
        this.x = this.lerp(this.x, player.x, 0.02);
      }else if(att_ida){
        this.x = this.lerp(this.x, player.x, 0.06);
        this.y = this.lerp(this.y, player.y, 0.06);
        att_ida = false;
      }else{
        this.x = this.lerp(this.x, player.x+(d*(this.sprite.direction)), 0.06);
        this.y = this.lerp(this.y, player.y+(d*(this.sprite.direction)), 0.06);
        att_ida = true;
      }
    }
    
    checkCollisionWalls(cw, ch, camera) {
      if ((this.x < camera.x - cw || this.x > camera.x + cw) ||
        (Math.abs(this.y - camera.y) > 125)) {
        this.active = false;
        this.dead = false;
      }
    }

    render(ctx) {
      if (this.active && (!this.dead)) {
        this.sprite.render(ctx);
      }
    }
  }

  GameEngine.Blader_enemy = Blader_enemy;
  return GameEngine;
})(GameEngine || {})