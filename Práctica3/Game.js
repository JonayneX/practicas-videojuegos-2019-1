var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  const PI2 = 2*Math.PI;

  let ini = "Presione ESPACIO comenzar.";
  let juego = "Asteroides by Jonayne";
  let vidas = 0;
  let pausa = true;
  let puntaje = 0;

  let t_spawn = 1.7;
  let aux_t_spawn = 0;

  let mejora= false;
  let t_mejora = 3;
  let aux_t_mejora = Math.random()*7;
  let mejora_x;
  let mejora_y;
  let mejora_size = 20;

  let KEY = GameEngine.KEY;
  
  let asteroidesIni = 1;

  let aPool;
  let bPool;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;

      this.ship = new GameEngine.Ship(cw/2, ch/2, 50);

      this.asteroidPool = new GameEngine.AsteroidPool(cw, ch);
      
      for(let i=0; i < asteroidesIni; i++)
        this.asteroidPool.addAsteroid();


      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      if(!pausa)
        this.ship.processInput();
      else{
        if (KEY.isPress(KEY.SPACE) && pausa) {
            pausa = false;
            vidas = 3;
            puntaje = 0;
            this.ship = new GameEngine.Ship(cw/2, ch/2, 50);
            this.asteroidPool = new GameEngine.AsteroidPool(cw, ch);
            for(let i=0; i < asteroidesIni; i++)
              this.asteroidPool.addAsteroid();

        }
      }
    }

    update(elapsed) {
      if(vidas <= 0){
        pausa = true;
      }

      if(!pausa){
        aux_t_mejora += elapsed;
        aux_t_spawn += elapsed;

        aPool = this.asteroidPool.asteroids;
        bPool = this.ship.weapon.bullets;

        if(aux_t_mejora >= t_mejora){
          mejora_x = Math.floor(Math.random()*cw);
          mejora_y = Math.floor(Math.random()*ch);
          mejora = true;
          aux_t_mejora = 0;
        }

        if(aux_t_spawn >= t_spawn && this.asteroidPool.asteroidsAlive() <= 8){
          console.log("HELP")
          this.asteroidPool.addAsteroid();
          aux_t_spawn = 0;
        }

        this.ship.update(elapsed);
        this.asteroidPool.update(elapsed);

        this.checkBorders(this.ship);

        if(mejora){
          let dist = Math.sqrt( (this.ship.x - mejora_x)*(this.ship.x - mejora_x) + (this.ship.y - mejora_y)*(this.ship.y - mejora_y) );
          if (dist < this.ship.radius + mejora_size/2) {
            mejora = false;
            this.ship.invulnerable = true;
          }
        }

        for (let i=0; i<aPool.length; i++) {
          if (aPool[i].isAlive) {
            this.checkBorders(aPool[i]);
          }
        }

        for (let i=0; i<bPool.length; i++) {
          if (bPool[i].isAlive) {
            this.checkBordersB(bPool[i]);
          }
        }

        if(!this.ship.invulnerable){
          for (let i=0; i<aPool.length; i++) {
            if(aPool[i].isAlive){
              if(this.checkCircleCollision(this.ship, aPool[i], "nave-asteroide")){
                vidas-=1;
                this.ship.invulnerable = true;
              }
            }
          }
        }

        for (let bullet_i=0; bullet_i<bPool.length; bullet_i++) {
          if (bPool[bullet_i].isAlive) {
            for (let asteroid_i=0; asteroid_i<aPool.length; asteroid_i++) {
              if ( (aPool[asteroid_i].isAlive) && (this.checkCircleCollision(bPool[bullet_i], aPool[asteroid_i], "bala-asteroide")) ) {
                bPool[bullet_i].isAlive = false;
                this.asteroidPool.split(asteroid_i, bPool[bullet_i]);
                if(aPool[asteroid_i].hp == 3)
                  puntaje += 20;
                else if(aPool[asteroid_i].hp == 2)
                  puntaje += 50;
                else
                  puntaje += 100;
              }
            }
          }
        }
      }
    }

    checkCircleCollision(obj1, obj2, tmpmsg) {
      let dist = Math.sqrt( (obj1.x - obj2.x)*(obj1.x - obj2.x) + (obj1.y - obj2.y)*(obj1.y - obj2.y) );
      if (dist < obj1.radius + obj2.radius) {
        console.log("colision", tmpmsg);
        return true;
      }
      return false;
    }

    checkBorders(gameObject) {
      if (gameObject.x < -gameObject.radius) {
        gameObject.x = cw + gameObject.radius;
      }
      if (gameObject.x > cw+gameObject.radius) {
        gameObject.x = -gameObject.radius;
      }
      if (gameObject.y < -gameObject.radius) {
        gameObject.y = ch + gameObject.radius;
      }
      if (gameObject.y > ch+gameObject.radius) {
        gameObject.y = -gameObject.radius;
      }
    }

    checkBordersB(bullet) {
      if (bullet.x < -bullet.radius || bullet.x > cw+bullet.radius 
        || bullet.x > cw+bullet.radius || bullet.y < -bullet.radius
        || bullet.y > ch+bullet.radius) {
        bullet.isAlive = false;
      }
    }

    render() {
      if(!pausa){
        this.ctx.clearRect(0, 0, cw, ch);

        this.ctx.fillStyle = "rgba(0,0,0,1)";
        this.ctx.fillRect(0, 0, cw, ch);

        this.ship.render(this.ctx);

        this.asteroidPool.render(this.ctx);

        if(mejora){
          this.ctx.fillStyle = "rgb(198, 14, 226)";
          this.ctx.beginPath();
          this.ctx.arc(mejora_x, mejora_y, mejora_size, 0, PI2);
          this.ctx.fill();
        }

        //Pintamos el puntaje.
        this.ctx.fillStyle = "green";
        this.ctx.beginPath();
        this.ctx.font = "bold 35px Monospace";
        this.ctx.strokeText(puntaje.toString(), 10, 35);
        this.ctx.fillText(puntaje.toString(), 10, 35);

        //Pintamos las vidas.
        this.ctx.beginPath();
        this.ctx.strokeText(vidas.toString(), cw-40, 35);
        this.ctx.fillText(vidas.toString(), cw-40, 35);
        this.ctx.beginPath();
        this.ctx.strokeText("Vidas:", cw-180, 35);
        this.ctx.fillText("Vidas:", cw-180, 35);      

      }else{
        //Pintamos unos textos wapillos.
        this.ctx.font = "bold 40px Monospace";
        this.ctx.strokeText(ini, 200, 300);
        this.ctx.fillText(ini, 200, 300);

        this.ctx.fillStyle = "red";
        this.ctx.strokeStyle = "yellow";
        this.ctx.font = "bold 50px Monospace";
        this.ctx.strokeText(juego, 200, 100);
        this.ctx.fillText(juego, 200, 100);
      }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})