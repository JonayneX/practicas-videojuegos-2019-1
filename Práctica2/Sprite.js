var GameEngine = (function(GameEngine) {
  class Sprite {
    constructor(x, y, w, h, img_path) {
      this.x = x;
      this.y = y;
      this.vx = 0;
      this.vy = 0;
      this.w = w;
      this.h = h;

      this.image = null;
      if (img_path) {
        this.image = new Image();
        this.image.src = img_path;
      }
    }

    processInput() { }

    update(elapsed) {

      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
    }
    
    render(ctx) {
      if (this.image) {
        ctx.save();
        ctx.translate(this.x, this.y);
        ctx.drawImage(this.image, -this.w/2, -this.h/2, this.w, this.h);
        ctx.restore();
      }
    }
  }

  GameEngine.Sprite = Sprite;
  return GameEngine;
})(GameEngine || {})