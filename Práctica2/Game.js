var GameEngine = (function(GameEngine) {

  let pausa;
  let ini = "Presione ESPACIO comenzar.";
  let juego = "Juego de CIRCUS";

  let cw;
  let ch;

  let puntaje = 0;
  let vidas = 0;
  let KEY = GameEngine.KEY;

  let numGlobos = 15;

  class Game {
    constructor(ctx) {

      cw = ctx.canvas.width;
      ch = ctx.canvas.height;

      this.ctx = ctx;

      pausa = true;

      
      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });

    }

    processInput() {
        if(!pausa){

          this.balancin.processInput();

        }else{
          if (KEY.isPress(KEY.SPACE) && pausa) {
            pausa = false;
            vidas = 3;
            puntaje = 0;

            this.balancin = new GameEngine.Balancin((cw/2)-200, ch, "L");

            this.globosSup = []
            this.globosCen = []
            this.globosInf = []

            // Creamos los globos.
            for(let i =0; i < numGlobos; i++){
              this.globosSup.push(new GameEngine.Globo((48*i)+30 , 39, 30, 30, "izq", "#E60D0E", 9, true, 3));
              this.globosCen.push(new GameEngine.Globo((48*i)+30 , 79, 30, 30, "der", "#056BAD", 5, true, 9));
              this.globosInf.push(new GameEngine.Globo((48*i)+30 , 119, 30, 30, "izq", "#F3D405", 2, true, 13));
            }

            //Equilibristas.
            this.equilibristaD = new GameEngine.Equilibrista(this.balancin.x+190, this.balancin.y-25, 40, true);
            this.equilibristaI = new GameEngine.Equilibrista(0, ch-300, 40, false);
            this.equilibristaI.vx = 300;
          }
        }
    }


    update(elapsed) {
      if(vidas <= 0){
        pausa = true;

      }

      if(!pausa){

        this.balancin.update(elapsed, cw);

        for(let i = 0; i < numGlobos; i++){
          this.globosSup[i].update(elapsed, cw);
          this.globosCen[i].update(elapsed, cw);
          this.globosInf[i].update(elapsed, cw);
        }

        this.equilibristaI.update(elapsed, cw, ch, this.balancin.diag);
        this.equilibristaD.update(elapsed, cw, ch, this.balancin.diag);

        this.checkCollisionEqui(this.equilibristaI);
        this.checkCollisionEqui(this.equilibristaD);

        for(let i = 0; i < numGlobos; i++){
          this.checkCollision(this.equilibristaI, this.globosSup[i], elapsed);
          this.checkCollision(this.equilibristaD, this.globosSup[i], elapsed);
          this.checkCollision(this.equilibristaI, this.globosCen[i], elapsed);
          this.checkCollision(this.equilibristaD, this.globosCen[i], elapsed);
          this.checkCollision(this.equilibristaI, this.globosInf[i], elapsed);
          this.checkCollision(this.equilibristaD, this.globosInf[i], elapsed);
        }
      }

    }

    checkCollisionEqui(equi){
      if(!equi.enBalancin){

        if(equi == this.equilibristaI){

          var equi2 = this.equilibristaD;
        }else{
          var equi2 = this.equilibristaI;
        }

        if(equi.y  >= ch){
          vidas-=1;
          if(this.balancin.diag == "L"){
            if (equi == this.equilibristaI) {
              this.equilibristaD = new GameEngine.Equilibrista(this.balancin.x+190, this.balancin.y-25, 40, true);
              this.equilibristaI = new GameEngine.Equilibrista(0, ch-300, 40, false);
              this.equilibristaI.vx = 300;
            }else{
              this.equilibristaI = new GameEngine.Equilibrista(this.balancin.x+190, this.balancin.y-25, 40, true);
              this.equilibristaD = new GameEngine.Equilibrista(0, ch-300, 40, false);
              this.equilibristaD.vx = 300;
            }
          }else{
            if (equi == this.equilibristaI) {
              this.equilibristaD = new GameEngine.Equilibrista(this.balancin.x+10, this.balancin.y-25, 40, true);
              this.equilibristaI = new GameEngine.Equilibrista(0, ch-300, 40, false);
              this.equilibristaI.vx = 300;
            }else{
              this.equilibristaI = new GameEngine.Equilibrista(this.balancin.x+10, this.balancin.y-25, 40, true);
              this.equilibristaD = new GameEngine.Equilibrista(0, ch-300, 40, false);
              this.equilibristaD.vx = 300;
            }
          }
        }

        //Revisamos colisión con el balancín.
        if(this.balancin.diag == "L"){

          if(equi.x >= this.balancin.x && equi.x <= this.balancin.x+110){

            if(equi.y >= this.balancin.y-65  && equi.y <= this.balancin.y-60){

              //ALTURA MÁXIMA.
              this.cambiaEquilibrista(equi, equi2, 1000);
            }else if(equi.y >= this.balancin.y-59  && equi.y <= this.balancin.y-54){
              //ALTURA MÍNIMA.
              this.cambiaEquilibrista(equi, equi2, 800);
            }
          }

        }else{

          if(equi.x >= ((this.balancin.x+200)/2)-50 && equi.x <= this.balancin.x+240){

            if(equi.y >= this.balancin.y-65  && equi.y <= this.balancin.y-60){
              //ALTURA MÁXIMA.
              this.cambiaEquilibrista(equi, equi2, 1000);
            }else if(equi.y >= this.balancin.y-59  && equi.y <= this.balancin.y-54){
              //ALTURA MÍNIMA.
              this.cambiaEquilibrista(equi, equi2, 800);
            }
          }

        }

      }else{

        if(this.balancin.diag == "L"){
          equi.x = this.balancin.x+190; 
          equi.y = this.balancin.y-25;
        }else{
          equi.x = this.balancin.x+10;
          equi.y = this.balancin.y-25;
        }
        
      }
    }

    checkCollision(equi, globo, elapsed) {
      if(globo.alive){
        let dx = globo.x - equi.x,
            dy = globo.y - equi.y,
            dist = Math.sqrt(dx * dx + dy * dy);

        if (dist < equi.size + globo.size) {

          puntaje +=  globo.puntos;
          globo.alive = false;
          if(globo.direction == "izq"){
            equi.vy = equi.dire * 250;
            equi.vx = - (130 + (Math.floor(Math.random() * 200)));
          }else{
            equi.vy = equi.dire * 250;
            equi.vx = (130 + (Math.floor(Math.random() * 200)));
          }

        }
      }
    }

    cambiaEquilibrista(equi1, equi2, h){
      //Equi1 es el que cae al balancín, equi2 es el que saldrá volando wuuuuh!
      equi1.enBalancin = true;
      equi2.enBalancin = false;
      equi2.y = ch - 66;
      equi2.vy = -equi2.dire * h;

      if(this.balancin.diag == "L"){
        this.balancin.diag = "R";
        equi1.x= this.balancin.x+190; 
        equi1.y= this.balancin.y-25;
        equi2.vx = -(120 + (Math.floor(Math.random() * 160)));
      }else{
        this.balancin.diag = "L";
        equi1.x= this.balancin.x+10;
        equi1.y= this.balancin.y-25;
        equi2.vx = (120 + (Math.floor(Math.random() * 160)));
      }

    }

    render() {

        if(!pausa){

            this.ctx.clearRect(0, 0, cw, ch);

            //Pintamos el puntaje.
            this.ctx.fillStyle = "red";
            this.ctx.beginPath();
            this.ctx.font = "bold 50px Monospace";
            this.ctx.strokeText(puntaje.toString(), 10, 35);
            this.ctx.fillText(puntaje.toString(), 10, 35);

            //Pintamos las vidas.
            this.ctx.font = "bold 40px Monospace";
            this.ctx.beginPath();
            this.ctx.strokeText(vidas.toString(), cw-40, 35);
            this.ctx.fillText(vidas.toString(), cw-40, 35);
            this.ctx.beginPath();
            this.ctx.strokeText("Vidas:", cw-180, 35);
            this.ctx.fillText("Vidas:", cw-180, 35);

            // Pintamos el sube y baja.
            this.balancin.render(this.ctx);

            // Pintamos los cuadraditos.
            for(let i = 0; i < numGlobos; i++){
              this.globosSup[i].render(this.ctx);
              this.globosCen[i].render(this.ctx); 
              this.globosInf[i].render(this.ctx);
            }

            // Pintamos a los equilibristas.
            this.equilibristaI.render(this.ctx, this.balancin.x, this.balancin.y, this.balancin.diag);
            this.equilibristaD.render(this.ctx, this.balancin.x, this.balancin.y, this.balancin.diag);

        }else{

            //Pintamos unos textos wapillos.
            this.ctx.font = "bold 40px Monospace";
            this.ctx.strokeText(ini, 200, 300);
            this.ctx.fillText(ini, 200, 300);

            this.ctx.fillStyle = "red";
            this.ctx.strokeStyle = "yellow";
            this.ctx.font = "bold 50px Monospace";
            this.ctx.strokeText(juego, 200, 100);
            this.ctx.fillText(juego, 200, 100);
        }

    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})