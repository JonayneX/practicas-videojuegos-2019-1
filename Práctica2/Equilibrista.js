var GameEngine = (function(GameEngine) {

  let gravityValue = 10;
  let gravity = gravityValue;

  class Equilibrista extends GameEngine.Sprite {
    constructor(x, y, size, enBalancin) {
      super(x, y, size, size, "Chavon.png");

      this.size = size;
      this.speed = 30;
      this.vx = 0;
      this.vy = 0;
      this.enBalancin = enBalancin;
      this.dire = 1;
    }

    processInput() {
    }

    update(elapsed, cw, ch, bdiag) {

      if(!this.enBalancin){

        this.vy += gravity*this.dire;

        super.update(elapsed);

        if (this.x < this.size) {
          this.vx *= -1;
          this.x = this.size;
        }
        if (this.x > cw-this.size) {
          this.vx *= -1;
          this.x = cw-this.size;
        }

        if (this.y <= 0){
          this.y = this.size;
        }

      }

    }
    
    render(ctx) {
      super.render(ctx);
    }
  }

  GameEngine.Equilibrista = Equilibrista;
  return GameEngine;
})(GameEngine || {})