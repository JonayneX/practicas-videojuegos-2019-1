var GameEngine = (function(GameEngine) {
  let dx, dy;
  let tamw = 10;
  let tamh = 4;

  let tamw_2 = tamw/2;
  let tamh_2 = tamh/2;

  class Camera {
    constructor(x, y, stage_w, ch) {
      this.x = x;
      this.y = (y*16)+100;
      this.stage_w = stage_w;
      this.stage_h = ch;

      this.cameraTransition = false;
      this.step = 0;
      this.oldCameraPos = { x: 0, y: 0 };
      this.direction = 1;
    }

    applyTransform(ctx) {
      ctx.save();
      ctx.translate(parseInt(this.stage_w/2 -this.x), parseInt(this.stage_h/2 -this.y));
    }
    releaseTransform(ctx) {
      ctx.restore();
    }

    update(player, level) {
      dx = this.x - player.x;
      if(dx < 0){
        this.x = Math.min(level.w - this.stage_w/2, this.lerp(this.x, player.x, 0.25));
      }else{
        this.x = Math.max(this.stage_w/2, this.lerp(player.x, this.x, 0.25));
      }

      if(this.cameraTransition){
        this.step += 0.02;

        if (this.step > 1) {
          this.step = 1;
          this.cameraTransition = false;
        }

        this.y = Math.min(level.h - this.stage_h/2, this.lerp(this.oldCameraPos.y, this.oldCameraPos.y - (235*this.direction), this.step));
      }else{
        this.revisaCambioCamara(player, level);
      }

    }

    lerp(v0, v1, t) {
      return v0 + (v1 - v0)*t;
    }

    revisaCambioCamara(p, level){
      if(this.y - p.y >= 125) {
        this.cameraTransition = true;
        this.step = 0;
        this.direction = 1;
        this.oldCameraPos = { x: this.x, y: this.y };
      }else if(this.y - p.y <= -125){
        this.cameraTransition = true;
        this.step = 0;
        this.direction = -1;
        this.oldCameraPos = { x: this.x, y: this.y };
      }
    }

    render(ctx, vidas) {
      ctx.lineWidth = 2;
      ctx.strokeStyle = "black";
      ctx.fillStyle = "yellow";

      var vida;
 
      for(let i = 0; i<14; i++){
        vida = vidas[i];
        ctx.beginPath();
        if(vida.live)
          ctx.fillStyle = "yellow";
        else
          ctx.fillStyle = "black";

        ctx.fillRect((this.x - 110) - tamw_2, (this.y - 110 + (i*5)) - tamh_2, tamw, tamh);
        ctx.strokeRect((this.x - 110) - tamw_2, (this.y - 110 + (i*5)) - tamh_2, tamw, tamh);
      }

    }
  }

  GameEngine.Camera = Camera;
  return GameEngine;
})(GameEngine || {})