var GameEngine = (function(GameEngine) {
  let cw;
  let ch;

  let KEY = GameEngine.KEY;

  let time_paux = 0;
  let cuenta = false;

  class Game {
    constructor(ctx) {
      cw = ctx.canvas.width;
      ch = ctx.canvas.height;
      this.ctx = ctx;
      this.inicio = true;

      this.camera = new GameEngine.Camera(cw/2, ch/2, cw, ch);

      // Un pixel en el mapa de Tiled (x,y) es (17x, 16y) en el canvas.
      this.player = new GameEngine.Player((17*8), (16*129), 32, 32);
      this.level = new GameEngine.Level();

      this.image_start = new Image();
      this.image_start.src = "images/startscreen.jpeg";

      this.image_lvl1 = new Image();
      this.image_lvl1.src = "images/lvl1-cutman.jpeg";

      window.addEventListener("keydown", function(evt) {
        KEY.onKeyDown(evt.keyCode);
      });
      window.addEventListener("keyup", function(evt) {
        KEY.onKeyUp(evt.keyCode);
      });
    }

    processInput() {
      this.player.processInput();
      if (KEY.isPress(KEY.ENTER_KEY)) {
        this.inicio = false;
        this.cutman_presen = true;
        cuenta = true;
      }
    }

     update(elapsed) {

      if(cuenta)
        time_paux += elapsed;
      if(!this.camera.cameraTransition){
        if(!this.inicio && !this.cutman_presen){
          if((this.player.tryGrabLadder) && (!this.player.inLadder)) {
                this.checkCollisionLadders(this.player, this.level);
              }
        
          this.player.update(elapsed);
    
          if (this.player.inLadder) {
            this.checkInLadders(this.player, this.level);
          }
    
          this.checkCollisionPlatforms(this.player, this.level);
    
          if (this.player.inFloor) {
            this.player.inLadder = false;
          }
    
          this.player.setState();
    
          this.checkCollisionWalls();
        }

      }
      this.camera.update(this.player, this.level); 
    }

    checkCollisionLadders(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);
      let ladder;

      // top
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y-1);
      if (ladder && (ladder.type === "Ladder") && (player.ladderVy < 0)) {
        this.player.x = ladder.x;
        this.player.y -= 4;
        this.player.inLadder = true;
        return;
      }

      //center
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y);
      if (ladder && (ladder.type === "Ladder")) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }

      // bottom
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y+1);
      if (ladder && (ladder.type === "Ladder") && (player.ladderVy > 0)) {
        this.player.x = ladder.x;
        this.player.inLadder = true;
        return;
      }
    }

    checkInLadders(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y + player.h_2);
      let ladder;
      //center
      ladder = level.getPlatform(player_tile_pos.x, player_tile_pos.y);
      if (ladder && (ladder.type === "Ladder")) {
        this.player.inLadder = true;
        this.player.w = 32;
        this.player.w_2 = 8;
      }
      else {
        this.player.inLadder = false;
        this.player.w = 32;
        this.player.w_2 = 10;
      }
      
    }

    checkCollisionWalls() {
      if (this.player.x < this.camera.x - cw/2 + this.player.w_2) {
        this.player.x = this.camera.x - cw/2 + this.player.w_2;
      }
      if (this.player.x > this.camera.x +cw/2 -this.player.w_2) {
        this.player.x = this.camera.x +cw/2 - this.player.w_2;
      }

    }

    checkCollisionPlatforms(player, level) {
      let player_tile_pos = level.getTilePos(player.x, player.y);
      for(let i = -1; i <= 1; i++){
        for(let j = -1; j <= 1; j++){
          this.reactCollision(player, level.getPlatform(player_tile_pos.x+i, player_tile_pos.y+j));
        }

      }
      
    }

    reactCollision(player, platform, inStairs) {
      if ( platform && platform.type === "Platform" &&
           Math.abs(player.x - platform.x) < player.w_2 + platform.w_2 && 
           Math.abs(player.y - platform.y) < player.h_2 + platform.h_2 ) {

        let overlapX = (player.w_2 + platform.w_2) - Math.abs(player.x - platform.x);
        let overlapY = (player.h_2 + platform.h_2) - Math.abs(player.y - platform.y);

        if (overlapX < overlapY) {
          if (player.x - platform.x > 0) {
            player.x += overlapX;
          }
          else {
            player.x -= overlapX;
          }
        }
        else if (overlapX > overlapY) {
          if (player.y - platform.y > 0) {
            player.y += overlapY;
            if (player.vy < 0) {
              player.vy = 0;
            }
          }
          else {
            player.y -= overlapY;
            if (player.vy > 0) {
              player.inFloor = true;
              player.vy = 0;
            }
          }
        }
      }
    }

    render() {
      if(!this.inicio && !this.cutman_presen){
        this.ctx.fillStyle = "#5ad0e1";
        this.ctx.fillRect(0, 0, cw, ch);
  
        this.camera.applyTransform(this.ctx);
  
        this.level.render(this.ctx);
        this.player.render(this.ctx);
        
        this.camera.render(this.ctx, this.player.vidas);
        this.camera.releaseTransform(this.ctx);

      }else if(this.inicio && !this.cutman_presen){

        if (this.image_start)
          this.ctx.drawImage(this.image_start, 0, 0);

      }else{

        if(this.image_lvl1)
          this.ctx.drawImage(this.image_lvl1, 0, 0);
        if(time_paux >= 4){
          cuenta = false;
          this.cutman_presen = false;
        }

      }
    }
  }

  GameEngine.Game = Game;
  return GameEngine;
})(GameEngine || {})