var GameEngine = (function(GameEngine) {
  
  let KEY = GameEngine.KEY;

  class Balancin {
    constructor(x, y, diag, delayActivation=0.8) {
      this.x = x;
      this.y = y;
      this.diag = diag;
      this.delayActivation = delayActivation;
      this.auxDelayTime = delayActivation;
    }

    processInput() {

      if (KEY.isPress(KEY.LEFT)) {
        this.x -= 9;
      }
      if (KEY.isPress(KEY.RIGHT)) {
        this.x += 9;  
      }
      if (KEY.isPress(KEY.Z)) {
        
        if(this.delayActivation < this.auxDelayTime){
          if(this.diag == "R"){
            this.diag = "L";
          }else{
            this.diag = "R";
          }
          this.auxDelayTime = 0;
        }
      }
    }

    update(elapsed, cw) {
      this.auxDelayTime += elapsed;

      if(this.x <= 0){
        this.x = 0;
      }
      if(this.x >= cw - 200){
        this.x = cw - 200;
      }
      
    }

    render(ctx) {
      ctx.beginPath();
      ctx.lineWidth = 7;

      if(this.diag == "R"){
        ctx.moveTo(this.x, this.y);
        ctx.lineTo(this.x+200, this.y-50);
      }else{
        ctx.moveTo(this.x+200, this.y);
        ctx.lineTo(this.x, this.y-50);
      }
      ctx.stroke();
    }


  }

  GameEngine.Balancin = Balancin;
  return GameEngine;
})(GameEngine || {})