var GameEngine = (function(GameEngine) {
  
  class Globo {
    constructor(x, y, weidth, height, direction, color, puntos, alive, velocity) {
      this.x = x;
      this.y = y;
      this.w = weidth;
      this.h = height;
      this.direction = direction;
      this.puntos = puntos;
      this.colors = color;
      this.size = 10;
      this.alive = alive;
      this.velocity = velocity;
    }


    update(elapsed, cw) {
      if(this.alive){
        if(this.direction == "der"){
          this.x += this.velocity;
        }else{
          this.x -= this.velocity;
        }
        if(this.x > cw){
          this.x = 0;
        }
        if(this.x < 0){
          this.x = cw;
        }
      }
    }

    render(ctx) {
      if(this.alive){
        ctx.fillStyle = this.colors;
        ctx.beginPath();
        ctx.fillRect(this.x - this.w/2 , this.y , this.w, this.h);
        ctx.fill(); 
      } 
    }


  }

  GameEngine.Globo = Globo;
  return GameEngine;
})(GameEngine || {})