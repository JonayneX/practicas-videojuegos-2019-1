var GameEngine = (function(GameEngine) {
  
  class Asteroid extends GameEngine.Sprite {
    constructor(x, y, size) {
      super(x, y, size, size, "asteroid.png");

      this.isAlive = false;
      this.x = x;
      this.y = y;
      this.speed = 150;

      this.angle = (360 * Math.random()) * Math.PI/180;
      this.vx = Math.cos(this.angle) * this.speed;
      this.vy = Math.sin(this.angle) * this.speed;
      this.size = this.radius = size;
      this.hp = 3;
    }

    hit() {
      this.hp--;

      if (this.hp > 0) {
        this.radius = this.size = super.w = super.h = (this.size/2);
        this.vx *= 1.1;
        this.vy *= 1.1;
      }
      else {
        this.isAlive = false;
        this.hp = 3;
        this.angle = (360 * Math.random()) * Math.PI/180;
        this.vx = Math.cos(this.angle) * this.speed;
        this.vy = Math.sin(this.angle) * this.speed;
      }
    }

    activate(x, y) {
      this.x = x;
      this.y = y;
      this.isAlive = true;
    }

    update(elapsed) {
      this.x += this.vx * elapsed;
      this.y += this.vy * elapsed;
      
    }

    render(ctx) {
      super.render(ctx);
      
    }
  }

  GameEngine.Asteroid = Asteroid;
  return GameEngine;
})(GameEngine || {})